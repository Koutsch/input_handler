/**
	Copyright © 2020 Jakob Wiedner
	
	This file is part of InputHandler.

	InputHandler is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Foobar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
	TODO
	1) check all functions
	2) check also IE ecma5-ish compatibility for at least ie 11
 */

"use strict";
var quillJsId = 'quill_js';
var quillStyleId = 'quill_style';
var signaturePadId = 'signature_pad_js';
var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],
  [{ 'indent': '-1'}, { 'indent': '+1' }],
  [{ 'direction': 'rtl' }],

  [{ 'size': ['small', false, 'large', 'huge'] }],
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean'],
  ['link'],
  ['video']
];

function addCustomQuill()
{
	var BlockEmbed = Quill.import('blots/block/embed');
	class VideoBlot extends BlockEmbed {
		static create(url) {
		  let node = super.create();
		  node.setAttribute('src', url);
		  node.setAttribute('frameborder', '0');
		  node.setAttribute('allowfullscreen', true);
		  return node;
		}

		static formats(node) {
		  let format = {};
		  if (node.hasAttribute('height')) {
		    format.height = node.getAttribute('height');
		  }
		  if (node.hasAttribute('width')) {
		    format.width = node.getAttribute('width');
		  }
		  return format;
		}

		static value(node) {
		  return node.getAttribute('src');
		}

		format(name, value) {
			if (name === 'height' || name === 'width') {
			  if (value) {
			    this.domNode.setAttribute(name, value);
			  } else {
			    this.domNode.removeAttribute(name, value);
			  }
			} else {
			  super.format(name, value);
			}
		}
	}
	VideoBlot.blotName = 'video';
	VideoBlot.tagName = 'iframe';
	Quill.register(VideoBlot);
	
	var Link = Quill.import('formats/link');
	class NoTargetLink extends Link {
		static create(value) {
		  const node = super.create(value);
		  node.removeAttribute('target');
		  return node;
		}
	}
	Quill.register(NoTargetLink, true);
}

var quillScript = document.createElement('script');
document.head.appendChild(quillScript);
quillScript.id = quillJsId;
quillScript.src = 'quill/quill.js';
quillScript.onload = function() {

	addCustomQuill();

	var signaturePadScript = document.createElement('script');
	document.head.appendChild(signaturePadScript);
	signaturePadScript.id = signaturePadId;
	signaturePadScript.src = 'signature_pad/dist/signature_pad.umd.min.js';
	signaturePadScript.onload = function() {
		var quillCssLink = document.createElement('link');
		document.head.appendChild(quillCssLink);
		quillCssLink.id = quillStyleId;
		quillCssLink.rel = 'stylesheet';
		quillCssLink.href = 'quill/quill.snow.css';
	}	
};

function InputHandler(inputDescJSON, inputLang, emptyLabel, fileTypeLabel, oldFileLabel, clearLabel, fullBar)
{
	fullBar = (fullBar === undefined) ? false : fullBar;
	let self = this;
	this.attributes = {
		containerDivAttributes: null,
		categoryLabelAttributes: null,
		inputLabelAttributes: null,
		inputAttributes: null,
		selectAttributes: null,
		deleteButtonAttributes: null,
		addButtonAttributes: null,
		hintAttributes: null,
		signatureWidth: 600,
		signatureHeight: 200,
		signatureWrapperAttributes: null,
		categoryDivAttributes: null,
		inputDivAttributes: null
	};
	this.inputObject = null;
	this.uniqueId = false;
	this.named = false;
	this.callback = null;
	
	this.setUniqueId = function(idValue)
	{
		this.uniqueId = idValue;
	}
	
	this.setNamed = function(named)
	{
		this.named = named;
	}
	
	this.setCallback = function(callback)
	{
		this.callback = callback;
	}
	
	this.setInputAttributes = function(
		containerDivision,
		categoryLabel,
		inputLabel, 
		input,
		select,
		deleteButton,
		addButton,
		hint,
		signatureWidth,
		signatureHeight,
		signatureWrapper,
		categoryDiv,
		inputDiv
	)
	{
		this.attributes.containerDivAttributes = containerDivision;
		this.attributes.categoryLabelAttributes = categoryLabel;
		this.attributes.inputLabelAttributes = inputLabel;
		this.attributes.inputAttributes = input;
		this.attributes.selectAttributes = select;
		this.attributes.deleteButtonAttributes = deleteButton;
		this.attributes.addButtonAttributes = addButton;
		this.attributes.hintAttributes = hint;
		this.signatureWidth = (signatureWidth === undefined) ? this.signatureWidth : signatureWidth;
		this.signatureHeight = (signatureHeight === undefined) ? this.signatureHeight : signatureHeight;
		this.attributes.signatureWrapperAttributes = (signatureWrapper === undefined) ? this.signatureWrapperAttributes : signatureWrapper;		
		this.attributes.categoryDivAttributes = (categoryDiv === undefined) ? this.categoryDivAttributes : categoryDiv;
		this.attributes.inputDivAttributes = (inputDiv === undefined) ? this.inputDivAttributes : inputDiv;
	}
	
	this.createInputs = function(inputDataDiv, compareJSON)
	{
		compareJSON = (compareJSON === undefined) ? null : compareJSON;
		loadLibs(function() {
			self.inputObject = {};
			for (var i in inputDescJSON)
			{
				let inputContainers = createInput(inputDescJSON[i], self.inputObject, compareJSON);
				inputContainers.forEach(function(inputContainer) {
					inputDataDiv.appendChild(inputContainer);
				});
			}
			
			if (self.callback !== null) self.callback(self.inputObject);
		});
	}
	
	this.checkInputs = function()
	{
		let resultObject = {};
		let status = {};
		let files = {};
		let check = checkInput(this.inputObject, resultObject, files, false);
		
		if (check === true)
		{
			status.success = true;
			status.result = resultObject;
			status.files = files;
		}
		else
		{
			status.success = false;
			status.result = check;
		}
		
		return status;
	}
	
	this.uploadInputs = function(checked, url, param, callback, additionalData)
	{
		var formData = new FormData();
		formData.append(param, JSON.stringify(checked.result));
		if (additionalData !== undefined)
		{
			for (let i in additionalData) formData.append(i, additionalData[i]);
		}
		for (var file in checked.files) formData.append(file, checked.files[file]);
		var xhr = new XMLHttpRequest();
		xhr.open('POST', url, true);
		xhr.onload = function() {
			callback(this, xhr);
		};
		xhr.send(formData);
	}
	
	this.getCurrentInputs = function(callback)
	{
		let resultObject = {};
		let status = {};
		let files = {};
		let check = checkInput(this.inputObject, resultObject, files, true);		
		let number = 0;
		for (let f in files) number++;
		let counter = 0;
		if (number == 0)
		{
			callback(resultObject);
			return;
		}
		for (let f in files)
		{
			(function(file) {
        var name = file.file.name;
        var reader = new FileReader();  
        reader.onload = function(e) {  
          counter++;
          file.object.upload = name;
          file.object.converted = reader.result;
          if (number === counter)
          {
          	callback(resultObject);
          }
        }
        reader.readAsDataURL(file.file);
		  })(files[f]);
		}
	}
	
	this.createFilters = function(filterDiv)
	{
		let filterArr = [];		
		for (var i in inputDescJSON)
		{
			let childDescDiv = createFilter(inputDescJSON[i], filterArr);
			filterDiv.appendChild(childDescDiv);
		}
		return filterArr;
	}
	
	function loadLibs(callback)
	{
		let stateCheck = setInterval(() => {
			if (document.readyState === 'complete') {
				clearInterval(stateCheck);
				callback();
			}
		}, 100);
	}
	
	function setAttributes(element, attributes)
	{
		if (attributes !== null)
		{
			for (let name in attributes)
			{
				element.setAttribute(name, attributes[name]);
			}
		}
	}
	
	function createDependents(inputDesc, inputObject, value, dependentsDiv, saved)
	{
		saved = (saved === undefined) ? null : saved;
		if (inputObject.dependents !== undefined) delete inputObject.dependents;
		while (dependentsDiv.firstChild)
		{
			dependentsDiv.removeChild(dependentsDiv.lastChild);
		}
		for (let d = 0; d < inputDesc.dependents.length; d++)
		{
			let dependent = inputDesc.dependents[d];
			if (value === dependent.condition)
			{
				if (inputObject.dependents === undefined) inputObject.dependents = {};
				for (let i = 0; i < dependent.inputs.length; i++)
				{
					let input = dependent.inputs[i];
					let savedInput = null;
					if (saved !== null) savedInput = saved[input.name];
					let inputContainer = createInputContainer(input, inputObject.dependents, savedInput, false);
					dependentsDiv.appendChild(inputContainer);
				}
			}
		}
	}
	
	function createInputTag(inputDesc, inputObject, saved)
	{
		let inputTag;
		switch (inputDesc.type)
		{
			case "text":
				inputTag = document.createElement('input');
				inputTag.setAttribute('type','text');
				if (saved !== null) inputTag.setAttribute('value', saved);
				setAttributes(inputTag, self.attributes.inputAttributes);
				inputObject.tag = inputTag;
				break;
			case "boolean":
				inputTag = document.createElement('input');
				inputTag.setAttribute('type','checkbox');
				if (saved !== null && saved === true) inputTag.checked = true;
				setAttributes(inputTag, self.attributes.inputAttributes);
				inputObject.tag = inputTag;
				break;
			case "select":
				let selectTag = document.createElement('select');
				setAttributes(selectTag, self.attributes.selectAttributes);
				let option = document.createElement('option');
				option.setAttribute('value', "");
				option.innerHTML = emptyLabel;
				selectTag.appendChild(option);
				let savedSelect;				
				if (inputDesc.dependents !== undefined && inputDesc.dependents !== null && inputDesc.dependents.length > 0)
				{
					let savedDependents = null;
					if (saved !== null)
					{
						savedSelect = saved.value;
						savedDependents = saved.dependents;
					}
					inputTag = document.createElement("div");
					inputTag.appendChild(selectTag);
					let dependentsDiv = document.createElement("div");
					createDependents(inputDesc, inputObject, savedSelect, dependentsDiv, savedDependents);
					inputTag.appendChild(dependentsDiv);
					selectTag.addEventListener("change", function() {
						let value = selectTag.value;
						createDependents(inputDesc, inputObject, value, dependentsDiv); // HIER WEITER
					});
					
				}
				else
				{
					inputTag = selectTag;
					savedSelect = saved;
				}
				for (let i = 0; i < inputDesc.list.length; i++)
				{
					let item = inputDesc.list[i];
					option = document.createElement('option');
					option.setAttribute('value', item.value);		
					option.innerHTML = item.title[inputLang];
					if (savedSelect === item.value) option.setAttribute('selected','');
					selectTag.appendChild(option);
				}
				inputObject.tag = selectTag;
				break;
			case "textarea":
				inputTag = document.createElement('div');
				let quillContainer = document.createElement('div');
				inputTag.appendChild(quillContainer);
				let quillOptions = {
					theme: 'snow'
				};
				if (fullBar)
				{
					quillOptions["modules"] = {toolbar: toolbarOptions};
				}			
				var quill = new Quill(quillContainer, quillOptions);
				if (saved !== null) quill.setContents(saved.quill_contents);
				inputObject.quill = quill;
				inputObject.tag = inputTag;
				break;
			case "date":
				inputTag = document.createElement('input');
				inputTag.setAttribute('type','date');
				inputTag.setAttribute('placeholder','yyyy-mm-dd');
				if (saved !== null) inputTag.setAttribute('value', saved);
				setAttributes(inputTag, self.attributes.inputAttributes);
				inputObject.tag = inputTag;
				break;
			case "time":
				inputTag = document.createElement('input');
				inputTag.setAttribute('type','time');
				inputTag.setAttribute('placeholder','hh:mm');
				if (saved !== null) inputTag.setAttribute('value', saved);
				setAttributes(inputTag, self.attributes.inputAttributes);
				inputObject.tag = inputTag;
				break;
			case "file":
				inputTag = document.createElement('div');
				let fileInputTag = document.createElement('input');
				fileInputTag.setAttribute('type','file');
				setAttributes(fileInputTag, self.attributes.inputAttributes);
				inputTag.appendChild(fileInputTag);
				if (saved !== null)
				{
					let oldName;
					let savedName;
					if ("saved" in saved && "upload" in saved)
					{
						inputObject.converted = saved.converted;
						inputObject.upload = saved.upload;
						fileInputTag.addEventListener('change', function() {
							if ("converted" in inputObject) delete inputObject.converted;
						});
						oldName = saved.upload;
						savedName = saved.saved;
					}
					else
					{
						oldName = saved.old;
						savedName = saved.internal;
					}
					let oldNameWrapper = document.createElement('div');
					let oldNameLabel = document.createElement('label');
					setAttributes(oldNameLabel, self.attributes.hintAttributes);
					let labelSpan = document.createElement("span");
					labelSpan.innerHTML = oldFileLabel + ": " + oldName;
					oldNameLabel.appendChild(labelSpan);
					oldNameWrapper.appendChild(oldNameLabel);
					inputTag.appendChild(oldNameWrapper);
					inputObject.saved = savedName;					
					if (inputDesc.mandatory === false)
					{
						let removeCheckWrapper = document.createElement('div');
						let removeCheck = document.createElement('input');
						removeCheck.setAttribute('type','checkbox');
						let tagId = 'check' + savedName;
						removeCheck.setAttribute('id',tagId);						
						removeCheckWrapper.appendChild(removeCheck);
						let clearLabelLabel = document.createElement('label');
						clearLabelLabel.setAttribute('for',tagId);
						clearLabelLabel.innerHTML = clearLabel;
						removeCheckWrapper.appendChild(clearLabelLabel);
						inputTag.appendChild(removeCheckWrapper);
						inputObject.clearBox = removeCheck;
					}
				}				
				if ("mime" in inputDesc && inputDesc["mime"] !== "")
				{					
					let mimeWrapper = document.createElement('div');
					let mimeLabel = document.createElement('label');
					setAttributes(mimeLabel, self.attributes.hintAttributes);
					let mimeSpan = document.createElement("span");
					mimeSpan.innerHTML = fileTypeLabel + ": " + inputDesc.mime;
					mimeLabel.appendChild(mimeSpan);
					mimeWrapper.appendChild(mimeLabel);
					inputTag.appendChild(mimeWrapper);
				}
				if ("kb_size" in inputDesc && inputDesc["kb_size"] !== "")
				{					
					let kb_sizeWrapper = document.createElement('div');
					let kb_sizeLabel = document.createElement('label');
					setAttributes(kb_sizeLabel, self.attributes.hintAttributes);
					let kb_sizeSpan = document.createElement("span");
					kb_sizeSpan.innerHTML = "max. " + inputDesc.kb_size + "KB";
					kb_sizeLabel.appendChild(kb_sizeSpan);
					kb_sizeWrapper.appendChild(kb_sizeLabel);
					inputTag.appendChild(kb_sizeWrapper);
				}
				inputObject.fileInput = fileInputTag;
				inputObject.tag = inputTag;
				break;
			case "signature":
				inputTag = document.createElement('div');
				let wrapperTag = document.createElement('div');
				setAttributes(wrapperTag, self.attributes.signatureWrapperAttributes);
				wrapperTag.setAttribute("style","position: relative; width: " + self.attributes.signatureWidth + "px; height: " + self.attributes.signatureHeight + "px; -moz-user-select: none; -webkit-user-select: none; -ms-user-select: none; user-select: none;");
				let canvas = document.createElement('canvas');
				canvas.setAttribute("style","position: absolute; left: 0; top: 0; width:" + self.attributes.signatureWidth + "px; height:" + self.attributes.signatureHeight + "px; background-color: white;");
				canvas.setAttribute("width",self.attributes.signatureWidth + "px");
				canvas.setAttribute("height",self.attributes.signatureHeight + "px");
				let signaturePad = new SignaturePad(canvas);
				wrapperTag.appendChild(canvas);
				inputTag.appendChild(wrapperTag);
				inputObject.signaturePad = signaturePad;
				let clearButton = document.createElement("button");
				clearButton.innerHTML = clearLabel;
				setAttributes(clearButton, self.attributes.addButtonAttributes);
				clearButton.addEventListener('click', function() {
					signaturePad.clear();
				});
				inputTag.appendChild(clearButton);				
				if (saved !== null)
				{
					signaturePad.fromDataURL(saved);
				}
				inputObject.tag = inputTag;
				break;
			default:
				inputTag = document.createElement('div');
				inputObject.tag = inputTag;
				break;
		}		
		return inputTag;
	}
	
	function createInput(inputDesc, inputObject, compareJSON)
	{
		let inputContainers = [];
		if (compareJSON !== null && inputDesc.name in compareJSON && compareJSON[inputDesc.name].length > 0)
		{
			for (let c = 0; c < compareJSON[inputDesc.name].length; c++)
			{
				let compareDesc = compareJSON[inputDesc.name][c];
				let inputContainer = createInputContainer(inputDesc, inputObject, compareDesc, (compareJSON[inputDesc.name].length > 1));
				inputContainers.push(inputContainer);
			}
		}
		else
		{
			let inputContainer = createInputContainer(inputDesc, inputObject, null, false);
			inputContainers[0] = inputContainer;
		}		
		if (inputDesc.occurrence === "multiple")
		{
			let addButton = createAddButton(inputDesc, inputObject);
			inputContainers.push(addButton);
		}		
		return inputContainers;
	}

	function addInput(button, inputDesc, inputObject)
	{
		let inputContainer = createInputContainer(inputDesc, inputObject, null, true);
		button.parentElement.insertBefore(inputContainer, button);		
		if (inputObject[inputDesc.name].inputObjects.length === 2)
		{
			let deleteButton = inputObject[inputDesc.name].inputObjects[0].deleteButton;
			let div = inputObject[inputDesc.name].inputObjects[0].div;
			div.parentElement.insertBefore(deleteButton, div);
		}
	}

	function deleteInput(inputContainer, inputArray, inputObject, mandatory)
	{
		inputContainer.remove();
		let index = inputArray.indexOf(inputObject);
		inputArray.splice(index, 1);
		
		if ((mandatory || "tag" in inputObject) && inputArray.length < 2)
		{
			inputArray.forEach(function(inputObject) {
				inputObject.deleteButton.remove();
			});
		}
	}

	function createAddButton(inputDesc, inputObject)
	{
		let addButton = document.createElement('button');
		addButton.addEventListener("click", function() {
			addInput(addButton, inputDesc, inputObject);
		});
		if ("title" in inputDesc && inputLang in inputDesc.title) addButton.innerHTML = '+ ' + inputDesc.title[inputLang];
		else addButton.innerHTML = '+';
		setAttributes(addButton, self.attributes.addButtonAttributes);
		return addButton;
	}
	
	function createDeleteButton(inputContainer, inputArray, inputObject, mandatory)
	{
		let deleteButton = document.createElement('button');
		deleteButton.innerHTML = '&times;';
		deleteButton.addEventListener('click', function() {
			deleteInput(inputContainer, inputArray, inputObject, mandatory);
		});
		setAttributes(deleteButton, self.attributes.deleteButtonAttributes);
		return deleteButton;
	}

	function addInputObject(inputDesc, parentInputObject)
	{
		let name = inputDesc.name;
		let inputObject = {};
		if (!(name in parentInputObject))
		{
			parentInputObject[name] = {category: inputDesc, inputObjects: []};
		}
		parentInputObject[name].inputObjects.push(inputObject);
		return inputObject;
	}
	
	function createLabelledContainer(inputDesc, labelAttributes, divAttributes, mandatory, showHint)
	{
		showHint = (showHint === undefined) ? null : showHint;
		let wrapper = document.createElement('div');
		setAttributes(wrapper, divAttributes);
		let label = null;
		if ("title" in inputDesc)
		{
			label = document.createElement('label');
			setAttributes(label, labelAttributes);
			let mandatorySign = "";
			if (mandatory) mandatorySign = "*";
			let mandatorySpan = document.createElement("span");
			mandatorySpan.innerHTML = inputDesc.title[inputLang] + mandatorySign;
			label.appendChild(mandatorySpan);	
			wrapper.appendChild(label);
		}
		let container = document.createElement('div');
		if (self.named === true) container.setAttribute("name", inputDesc.name);
		setAttributes(container, self.attributes.containerDivAttributes);
		wrapper.appendChild(container);
		if (showHint && "hint" in inputDesc && inputDesc.hint[inputLang] !== "")
		{
			let hint = document.createElement('div');
			setAttributes(hint, self.attributes.hintAttributes);
			hint.innerHTML = inputDesc.hint[inputLang];
			if ("hint_position" in inputDesc && inputDesc.hint_position === "after") wrapper.appendChild(hint);
			else wrapper.insertBefore(hint, container);
		}		
		return {wrapper: wrapper, container: container, label: label};
	}

	function createInputContainer(inputDesc, parentInputObject, saved, createDelete)
	{
		let inputObject = addInputObject(inputDesc, parentInputObject);		
		let labelAttributes;
		let divAttributes;
		let childContainers = [];
		if ("inputs" in inputDesc)
		{
			if (saved !== null && self.uniqueId !== false && self.uniqueId in saved && saved[self.uniqueId] !== false) inputObject.uniqueId = saved[self.uniqueId];
			
			labelAttributes = self.attributes.categoryLabelAttributes;
			divAttributes = self.attributes.categoryDivAttributes;
			inputObject.inputObject = {};
			for (let i = 0; i < inputDesc.inputs.length; i++)
			{
				let input = inputDesc.inputs[i];
				let inputContainers = createInput(input, inputObject.inputObject, saved);
				childContainers = childContainers.concat(inputContainers);
			}
		}
		else
		{
			labelAttributes = self.attributes.inputLabelAttributes;
			divAttributes = self.attributes.inputDivAttributes;
			let inputTag = createInputTag(inputDesc, inputObject, saved);
			childContainers.push(inputTag);
		}
		let mandatory = ("mandatory" in inputDesc && inputDesc.mandatory === true);
		let labelledContainer = createLabelledContainer(inputDesc, labelAttributes, divAttributes, mandatory, true);
		inputObject.div = labelledContainer.container;
		inputObject.label = labelledContainer.label;		
    if (inputDesc.occurrence === "multiple")
    {
			let deleteButton = createDeleteButton(labelledContainer.wrapper, parentInputObject[inputDesc.name].inputObjects, inputObject, mandatory);			
      if ((!mandatory && "inputs" in inputDesc) || createDelete) labelledContainer.container.appendChild(deleteButton);
      inputObject.deleteButton = deleteButton;
    }
    for (let c = 0; c < childContainers.length; c++)
    {
    	let childContainer = childContainers[c];
    	labelledContainer.container.appendChild(childContainer);
    }
		return labelledContainer.wrapper;
	}

	function checkInput(inputObject, resultObject, files, skipEmptyCheck)
	{
		for (const name in inputObject)
		{
			let categoryObject = inputObject[name];
			let inputDesc = categoryObject.category;
			let mandatory = ("mandatory" in inputDesc && inputDesc.mandatory === true);
			if (!(skipEmptyCheck) && mandatory && categoryObject.inputObjects.length < 1) return null;			
			if (categoryObject.inputObjects.length > 0) resultObject[name] = [];
			for (let c = 0; c < categoryObject.inputObjects.length; c++)
			{
				let childInputObject = categoryObject.inputObjects[c];
				if ("inputs" in inputDesc)
				{
					let childResultObject = {};
					if ("uniqueId" in childInputObject) childResultObject[self.uniqueId] = childInputObject.uniqueId;
					
					resultObject[name].push(childResultObject);
					let checkedInput = checkInput(childInputObject.inputObject, childResultObject, files, skipEmptyCheck);
					if (checkedInput !== true) return checkedInput;
				}
				else
				{
					switch (inputDesc.type)
					{
						case "boolean":
							resultObject[name].push(childInputObject.tag.checked); 
							break;
						case "textarea":
							if (!(skipEmptyCheck) && mandatory && childInputObject.quill.getText().trim().length === 0) return childInputObject;
							let textContent = {};
							textContent["raw"] = childInputObject.quill.root.innerHTML;
							var quillContents = childInputObject.quill.getContents();
							textContent["quill_contents"] = quillContents;
							resultObject[name].push(textContent);
							break;
						case "file":
							let fileUpload = {saved: null, upload: null};
							if ("saved" in childInputObject) fileUpload.saved = childInputObject.saved;					
							if (childInputObject.fileInput.files.length > 0)
							{
								let file = childInputObject.fileInput.files[0];
								if (skipEmptyCheck) files[file.name] = {file: file, object: fileUpload};
								else files[file.name] = file;
								fileUpload.upload = file.name;
							}
							else if ("converted" in childInputObject)
							{
								fileUpload.converted = childInputObject.converted;
								fileUpload.upload = childInputObject.upload;
							}
							if (fileUpload.saved === null && fileUpload.upload === null && (fileUpload.converted === undefined || fileUpload.converted === null))
							{
								if (!(skipEmptyCheck) && mandatory) return childInputObject;
							}
							else
							{
								if ("clearBox" in childInputObject && childInputObject.clearBox.checked)
								{
									fileUpload.remove = "";
								}
								resultObject[name].push(fileUpload);
							}					
							break;
						case "signature":
							if (!(skipEmptyCheck) && mandatory && childInputObject.signaturePad.isEmpty()) return childInputObject;
							let dataURL = childInputObject.signaturePad.toDataURL();
							resultObject[name].push(dataURL);
							break;
						case "date":
							let date = new Date(childInputObject.tag.value);
							if (!(isNaN(date.getTime()))) resultObject[name].push(childInputObject.tag.value);
							else if (!(skipEmptyCheck) && mandatory) return childInputObject;
							break;
						case "time":
							let pattern = /[0-9]{2}:[0-9]{2}/;
							let res = pattern.test(childInputObject.tag.value);
							if (res === false) return childInputObject;
							let time = childInputObject.tag.value.split(":");
							let hour = parseInt(time[0]);
							let minute = parseInt(time[1]);
							if (hour < 0 || hour > 24) return childInputObject;
							if (minute < 0 || minute > 59) return childInputObject;
							if (hour === 24 && minute !== 0) return childInputObject;
							resultObject[name].push(childInputObject.tag.value);
							break;
						case 'select':
							if (childInputObject.tag.value !== "" && childInputObject.tag.value !== null && childInputObject.tag.value !== undefined)
							{
								if (inputDesc.dependents !== undefined)
								{
									let dependentResultObject = {};									
									if (childInputObject.dependents === undefined) childInputObject.dependents = [];									
									let checkedInput = checkInput(childInputObject.dependents, dependentResultObject, files, skipEmptyCheck);
									if (checkedInput !== true) return checkedInput;
									resultObject[name].push({value: childInputObject.tag.value, dependents: dependentResultObject});
								}
								else
								{
									resultObject[name].push(childInputObject.tag.value);
								}
							}
							else if (!(skipEmptyCheck) && mandatory) return childInputObject;
							break;
						default:							
							if (childInputObject.tag.value !== "" && childInputObject.tag.value !== null && childInputObject.tag.value !== undefined)
							{
								resultObject[name].push(childInputObject.tag.value);
							}
							else if (!(skipEmptyCheck) && mandatory) return childInputObject;
							break;
					}
				}
			}
		}
		return true;	
	}
	
	function createPath(inputDesc, pathArr)
	{
		let path = [];
		if (pathArr !== null)
		{
			for (let p = 0; p < pathArr.length; p++)
			{
				path.push(pathArr[p]);
			}
		}
		path.push(inputDesc.name);
		return path;
	}
	
	function createFilterInput(inputDesc)
	{
		let filterInput;
		switch (inputDesc.type)
		{
			case "select":
				filterInput = document.createElement('select');
				let option = document.createElement('option');
				option.setAttribute('value', "");
				option.innerHTML = emptyLabel;
				filterInput.appendChild(option);
				inputDesc.list.forEach(function(listItem) {
					option = document.createElement('option');
					option.setAttribute('value', listItem.value);					
					option.innerHTML = listItem.title[inputLang];
					filterInput.appendChild(option);
				});				
				break;
			case "text":
			case "textarea":
				filterInput = document.createElement("input");
				filterInput.setAttribute("type", "text");
				break;
			case "boolean":
				filterInput = document.createElement('select');
				let boolOption = document.createElement('option');
				boolOption.setAttribute('value', "");
				filterInput.append(boolOption);
				boolOption = document.createElement('option');
				boolOption.setAttribute('value', "1");
				boolOption.innerHTML = "&#x2611;";
				filterInput.append(boolOption);
				boolOption = document.createElement('option');
				boolOption.setAttribute('value', "0");
				boolOption.innerHTML = "&#x2610;";
				filterInput.append(boolOption);
				break;
			case "date":
				filterInput = document.createElement("input");
				filterInput.setAttribute('type','date');
				filterInput.setAttribute('placeholder','yyyy-mm-dd');
				break;
			case "time":
				filterInput = document.createElement("input");
				filterInput.setAttribute('type','time');
				filterInput.setAttribute('placeholder','hh:mm');
				break;
			default:
				break;
		}
		return filterInput;
	}
	
	function filterOnChange(filterArr, path, value)
	{
		let exists = false;
		let i = 0;
		for (; i < filterArr.length; i++)
		{
			if (path.length == filterArr[i].path.length) for (let j = 0; j < path.length; j++)
			{
				if (path[j] === filterArr[i].path[j]) exists = true;
				else exists = false;
			}
			if (exists) break;
		}
		if (exists)
		{
			if (value === "") filterArr.splice(i, 1);
			else filterArr[i].value = value;
		}
		else
		{
			filterArr.push({ "path": path, "value": value });
		}
	}
	
	function createFilter(inputDesc, filterArr, pathArr)
	{
		pathArr = (pathArr === undefined) ? null : pathArr;
		let path = createPath(inputDesc, pathArr);		
		let labelAttributes;
		let divAttributes;
		let childFilters = [];
		let container;		
		if ("inputs" in inputDesc)
		{
			labelAttributes = self.attributes.categoryLabelAttributes;
			divAttributes = self.attributes.categoryDivAttributes;
			for (let i = 0; i < inputDesc.inputs.length; i++)
			{
				childFilters.push(createFilter(inputDesc.inputs[i], filterArr, path));
			}
		}
		else
		{
			labelAttributes = self.attributes.inputLabelAttributes;
			divAttributes = self.attributes.inputDivAttributes;			
			let filterInput = createFilterInput(inputDesc);			
			filterInput.onchange = function(event) {
				if (inputDesc.type === "boolean")
				{
					let value;
					if (event.target.value === "1") value = true;
					else if (event.target.value === "0") value = false;
					else value = "";
					filterOnChange(filterArr, path, value);
				}
				else if (inputDesc.type === "select" && "dependents" in inputDesc)
				{
					let condition = event.target.value;
					let oldWrapper = event.target.parentElement.querySelector("div");
					if (oldWrapper !== null) oldWrapper.parentElement.removeChild(oldWrapper);
					for (let i = 0; i < inputDesc.dependents.length; i++)
					{
						if (condition === inputDesc.dependents[i].condition)
						{
							let wrapper = document.createElement("div");
							for (let j = 0; j < inputDesc.dependents[i].inputs.length; j++)
							{
								let input = createFilter(inputDesc.dependents[i].inputs[j], filterArr, path);
								wrapper.appendChild(input);
							}
							event.target.parentElement.appendChild(wrapper);
						}
						else
						{
							for (let j = 0; j < filterArr.length; j++)
							{
								if (filterArr[j].path.length > path.length)
								{
									let isDependent = true;
									for (let k = 0; k < path.length && isDependent; k++)
									{
										if (path[k] !== filterArr[j].path[k]) isDependent = false;
									}
									if (isDependent) filterArr.splice(j, 1);
								}								
							}
						}
					}
					filterOnChange(filterArr, path, event.target.value);
				}
				else filterOnChange(filterArr, path, event.target.value);
				
			};			
			childFilters.push(filterInput);
		}		
		container = createLabelledContainer(inputDesc, labelAttributes, divAttributes, false, false);
		for (let c = 0; c < childFilters.length; c++)
		{
			container.container.appendChild(childFilters[c]);
		}		
		return container.wrapper;
	}
}
