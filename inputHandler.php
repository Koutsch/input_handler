<?php
	/**
		Copyright © 2020 Jakob Wiedner
		
		This file is part of InputHandler.

		InputHandler is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		Foobar is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
	*/

	// constants
	define('DATE_FORMAT', 'Y-m-d', false);
	define('SUCCESS', 0, false);
	define('UPLOAD_ERR', 1, false);
	define('MANDATORY_NULL_ERR', 2, false);
	define('TEXTAREA_NO_RAW_DATA_ERR', 3, false);
	define('TEXTAREA_EMPTY_ERR', 4, false);
	define('SELECT_NOT_LISTED_ERR', 5, false);
	define('NO_DATE_ERR', 6, false);
	define('INVALID_FILE_ERR', 7, false);
	define('NO_FILE_ERR', 8, false);
	define('SIGNATURE_DECODE_ERR', 9, false);
	define('SIGNATURE_READ_ERR', 10, false);
	define('SIGNATURE_COLOR_ERR', 11, false);
	define('EMPTY_STR_ERR', 12, false);
	define('INVALID_INPUT_ERR', 13, false);
	define('MANDATORY_ERR', 14, false);
	define('BOOLEAN_ERR', 15, false);
	define('NO_DEPENDENTS_ERR', 16, false);
	define('CONDITION_ERR', 17, false);
	define('NO_COMPARE_ERR', 18, false);
	define('SIZE_ERR', 19, false);
	
	/**
	 * Class for checking the validity of an input JSON file. Works with InputHandler js.
	 * Dependencies: php-imagick
	 */
	class InputHandler
	{
		// the input description JSON an instantiation of InputHandler uses
		private $input_desc_json;
		// the directory to which upload file input
		private $file_upload_dir;
		// the list of files to be uploaded and/or replaced
		private $files = null;
		// a input JSON to compare the newly uploaded input JSON to
		private $compare_json = null;
		// a list of all files associated with the compare JSON
		private $compare_files = null;
		// unique id to add to every object
		private $unique_id = false;
		// the additional upload folder array
		private $upload_paths;
		
		/**
		 * Create a new input handler with description categories defined.
		 * @param input_desc_json the reference of the description categories JSON
		 * @param file_upload_dir the path of the file upload directory
		 * @param unique_id the key of a unique id to be added to every input object, false by default
		 */
		function __construct(&$input_desc_json, $file_upload_dir, $unique_id = false)
		{
			$this->input_desc_json = $input_desc_json;
			$this->file_upload_dir = $file_upload_dir;
			$this->unique_id = $unique_id;
		}		
		
		/**
		 * Trims whitespace characters from beginning and end
		 * @param str the string to be cleaned
		 * @return the new clean string
		 */
		public function clean_string($str)
		{
			return trim($str);
		}
		
		/**
		 * Checks the input JSON given against the input description.
		 * The input JSON given will be altered and becomes the valid input for storing.
		 * If there is file input the files are uploaded to the file upload directory. By giving another input JSON for comparison, the files associated will be replaced and removed so that the new input JSON replaces the old one to be compared to.
		 * @param input the reference the the associative array representing the input JSON
		 * @param compare_json the reference to another input JSON to compare the input JSON to
		 */
		public function check_inputs(&$input, &$compare_json = null)
		{
			// set files array
			$this->files = array();
			$this->compare_json =& $compare_json;
			$this->compare_files = array();
			if ($compare_json !== null) $this->get_assoc_files($this->compare_json, $this->compare_files);
			
			// make the check
			$status = $this->check_input($this->input_desc_json, $input);
			
			// if successful upload files
			if ($status === SUCCESS) 
			{
				$status = $this->upload_files();
			}
			
			return $status;
		}
		
		/**
		 * Get the names of all associated files.
		 * @param input the input JSON to go through
		 */
		public function get_assoc_filenames(&$input)
		{
			if ($input === null) return null;
			$files = array();
			$this->get_assoc_files($input, $files, true);
			$filenames = array();
			foreach ($files as $file)
			{
				array_push($filenames, $file);
			}
			return $filenames;
		}

		/**
		 * Go through an input JSON and remove all files found.
		 * @param input the input JSON to go through
		 */
		public function remove_assoc_files(&$input)
		{
			$files_to_remove = array();
			$this->get_assoc_files($input, $files_to_remove);
			
			foreach ($files_to_remove as $file)
			{
				$this->unlink_file($file["internal"]);
			}
		}
		
		/**
		 * Add an additional path for an alternative file upload folder. It can be used in the input description with the 'upload' parameter.
		 * @param name the key of the uplaod folder to be used in the input description
		 * @param path the upload folder path
		 */
		public function add_additional_upload_path($name, $path)
		{
			$this->upload_paths[$name] = $path;
		}
		
		private function unlink_file($filename)
		{
			$file_path = $this->file_upload_dir . DIRECTORY_SEPARATOR . $filename;
			if (file_exists($file_path)) $removed = unlink($file_path);
			foreach ($this->upload_paths as $name => $path)
			{
				$file_path = $path . DIRECTORY_SEPARATOR . $filename;
				if (file_exists($file_path))
				{
					$removed = unlink($file_path);
					if ($removed) return true;
				}
			}
			return false;
		}

		/**
		 * Checks if date is correct.
		 * @param date the date string
		 * @param format the expected format
		 * @return true if valid date string, false othewise
		 */
		private function validate_date($date, $format = DATE_FORMAT)
		{
			$d = DateTime::createFromFormat($format, $date);
			return $d && $d->format($format) === $date;
		}
		
		/**
		 * Checks if the mime type is correct.
		 * @param desc the mime type to check against
		 * @param file the mime type of the file
		 * return true if same mime type, false otherwise
		 */
		private function check_mime($desc, $file)
		{
			$desc_split = explode("/", $desc);
			$file_split = explode("/", $file);
			for ($i = 0; $i < count($desc_split); $i++)
			{
				if ($desc_split[$i] !== $file_split[$i]) return false;
			}
			return true;
		}
		
		/**
		 * Check an uploaded file for vaidity.
		 * @param file the file uploaded to check
		 * @param true if file is valid, false otherwise
		 */
		private function check_file($file, $mime = null)
		{
			// Check for: undefined, multiple files, corruption
			if (!isset($file['error']) || is_array($file['error']))
			{
				return false;
			}
			
			// Check for: file error
			switch ($file['error'])
			{
				case UPLOAD_ERR_OK:
				  break;
				case UPLOAD_ERR_NO_FILE:
				  return false; // NO FILE
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
				  return false; // WRONG SIZE
				default:
				  return false; // ANY ERROR
			}
			
			if ($mime !== null) 
			{
				// get mime type from 
				$finfo = new finfo(FILEINFO_MIME_TYPE);
				$mime_type = $finfo->file($file['tmp_name']);
				if (!($this->check_mime($mime, $mime_type))) return false;
			}
			
			// if no error
			return true;
		}
		
		/**
		 * Store files uploaded on server.
		 * @return SUCCESS if all files have been updated successfully or another integer representing an error
		 */
		private function upload_files()
		{
			// check if files are set
			if ($this->files === null || count($this->files) == 0) return SUCCESS;
			
			// create upload directory if not exists
			if (!(file_exists($this->file_upload_dir))) mkdir($this->file_upload_dir);
			
			// upload new files
			$upload_success = true;
			foreach ($this->files as $name => $file)
			{
				if ($file !== 0 && array_key_exists("upload", $file) && $file["upload"] !== null && $file["upload"] !== 0)
				{
					// check if additional folder
					if (array_key_exists("folder", $file)) $folder = $file["folder"];
					else $folder = $this->file_upload_dir;
					// check if string
					if (array_key_exists("filename", $file))
					{
						file_put_contents($folder . DIRECTORY_SEPARATOR . $file["filename"], $file["upload"]);
					}
					else
					{
						if (!(move_uploaded_file(
							$file["upload"]['tmp_name'],
							$folder . DIRECTORY_SEPARATOR . $name
						)))
						{
							$upload_success = false;
							break;
						}
					}	
				}
			}
			
			// if there was an error uploading the files, 
			// delete all files uploaded so far and return error
			if (!($upload_success))
			{
				foreach ($this->files as $name => $file)
				{
					if ($file["upload"] !== null && $file["upload"] !== 0) $this->unlink_file($name);
				}
				return UPLOAD_ERR;
			}
			
			// delete all files of compare JSON if not among newly uploaded files
			if ($this->compare_json !== null)
			{				
				// loop old files
				foreach ($this->compare_files as $old_file)
				{
					// delete old file if not among new
					if (!(array_key_exists($old_file["internal"], $this->files)))
					{
						$this->unlink_file($old_file["internal"]);
					}
				}
			}
			
			// delete all files replaced by a newly uploaded file or which are to be removed
			foreach ($this->files as $name => $file)
			{
				if (
					$file !== 0 &&
					array_key_exists("upload", $file) &&
					array_key_exists("saved", $file) &&
					($file["upload"] !== null && $file["upload"] !== 0) &&
					($file["saved"] !== null && $file["saved"] !== 0 && $file["saved"] !== "")
				)
				{
					$this->unlink_file($file["saved"]);
				}
				
				if ($file !== 0 && array_key_exists("remove", $file) && $file["remove"] !== null)
				{
					$this->unlink_file($file["remove"]);
				}
			}
			
			return SUCCESS;
		}
		
		/**
		 * Get all file objects from an input JSON.
		 * @param input the input JSON to go through
		 * @param file_array an array raference to copy the file objects to
		 * @param add_path if true, the file location is added
		 * @input_desc_json the input description to use, if null is given the input description loaded will be used (default: null)
		 */
		private function get_assoc_files(&$input, &$file_array, $add_path=false, &$input_desc_json = null)
		{
			// assign input description if not given as parameter
			if ($input_desc_json === null) $input_desc_json = $this->input_desc_json;
			
			// loop description
			foreach ($input_desc_json as &$input_desc)
			{
				// check if exists in input
				if (array_key_exists($input_desc["name"], $input))
				{
					// loop input with name of input description
					foreach ($input[$input_desc["name"]] as &$field)
					{
						if (array_key_exists("inputs", $input_desc))
						{
							// go to next level
							$this->get_assoc_files($field, $file_array, $add_path, $input_desc["inputs"]);
						}
						else if ($input_desc["type"] === "file")
						{
							// add to array
							if ($add_path) array_push($file_array, array("file" => $field, "path" => (array_key_exists("upload", $input_desc) ? $input_desc["upload"] : "")));
							else array_push($file_array, $field);
						}
					}
				}
			}
		}
		
		/**
		 * Check a single data object field of a input description.
		 * @param input_desc the input description describing to the field
		 * @param field the data object field to check
		 */
		private function check_field(&$input_desc, &$field, &$parent)
		{		
			// check if mandatory data but empty
			if ($field === null && $input_desc["mandatory"] === true) return MANDATORY_NULL_ERR;			
			
			// check by type
			switch ($input_desc["type"])
			{
				case "boolean":
					if ($field === true || $field === false) return SUCCESS;
					return BOOLEAN_ERR;
				case "textarea":
					if (!(array_key_exists("raw", $field))) return TEXTAREA_NO_RAW_DATA_ERR;
					preg_match('/(>[^<]+<)/', $field["raw"], $matches);
					if ($input_desc["mandatory"] === true && count($matches) == 0) return TEXTAREA_EMPTY_ERR;
					return SUCCESS;
				case 'select':
					// check if dependent
					$value = $field;
					$dependents = null;
					if (array_key_exists("dependents", $input_desc))
					{
						$value = $field["value"];
						// return error if no dependents
						if (!(array_key_exists("dependents", $field))) return NO_DEPENDENTS_ERR;
						$dependents =& $field["dependents"];
					}					
					// check if in list
					if (false === (array_search($value, array_column($input_desc["list"], "value")))) return SELECT_NOT_LISTED_ERR;
					// check dependents if exist
					if ($dependents !== null)
					{
						// get dependent description for selected item
						foreach ($input_desc["dependents"] as $dependent)
						{
							if ($dependent["condition"] === $value)
							{
								// check dependents
								$status = $this->check_input($dependent["inputs"], $dependents);
								if ($status !== SUCCESS) return $status;
								break;
							}
						}
						return SUCCESS;
					}
					return SUCCESS;
				case 'date':
					// check if valid date string
					if(!($this->validate_date($field))) return NO_DATE_ERR;
					return SUCCESS;
				case 'file':
					// get file info from client
					$saved = (array_key_exists("saved", $field) && $field["saved"] !== null && $field["saved"] !== "" ? htmlspecialchars($field["saved"]) : null);
					$upload = (array_key_exists("upload", $field) && $field["upload"] !== null && $field["upload"] !== "" ? htmlspecialchars($field["upload"]) : null);
					$converted = (array_key_exists("converted", $field) && $field["converted"] !== null && $field["converted"] !== "" ? $field["converted"] : null);
					$remove = array_key_exists("remove", $field);
					
					// check if to remove existing file
					if ($remove && $saved !== null && $input_desc["mandatory"] !== true)
					{
						// check if compare file given
						if (count($this->compare_files) == 0) return NO_COMPARE_ERR;
						// set field to null
						$field = null;
						// find file to delete in inputs compared
						foreach ($this->compare_files as $assoc_file)
						{
							if ($assoc_file['internal'] === $saved)
							{
								$this->files[$assoc_file['internal']] = array("remove" => $saved); // add to list for later removal
								return SUCCESS;
							}
						}
					}
					else if ($upload === null && $converted === null) // check if no new upload is given and saved really exists
					{
						if ($saved !== null && $this->compare_json !== null)
						{							
							foreach ($this->compare_files as $assoc_file)
							{
								if ($assoc_file['internal'] === $saved)
								{
									// set old file as new file
									$field = array("internal" => $assoc_file["internal"], "old" => $assoc_file["old"]);
									$mime = null;
									if (array_key_exists("mime", $input_desc)) $mime = $input_desc["mime"];
									if ($mime !== null) $field["mime"] = $mime;
									$this->files[$assoc_file["internal"]] = 0; // add to list for later check
									return SUCCESS;
								}
							}
						}
					}		
					else // check new file and replace by old if exists
					{
						// convert data
						if ($converted !== null)
						{
							if ($upload === null) return NO_FILE_ERR;
							// remove mime type
							$file_parts = explode(",", $converted);
							$converted_only = $file_parts[1];
							// decode file
							$file = base64_decode($converted_only, true);
							if ($file === false) return INVALID_FILE_ERR;
							// check size
							if (array_key_exists("kb_size", $input_desc) && $input_desc["kb_size"] !== "")
							{
								if ((strlen($file) / 1000) > intval($input_desc["kb_size"])) return SIZE_ERR;
							}
							// check type
							$mime = null;
							if (array_key_exists("mime", $input_desc) && $input_desc["mime"] !== "") $mime = $input_desc["mime"];
							if ($mime !== null)
							{
								$mime_type = str_replace(";base64", "", str_replace("data:", "", $file_parts[0]));
								if (!($this->check_mime($mime, $mime_type))) return INVALID_FILE_ERR;
							}
							// create new internal name for file
							$new_filename = sprintf('%s%d', sha1($file), time());
							// add new info to field
							$field = array("internal" => $new_filename, "old" => $upload);
							// add to files for later processing
							$this->files[$new_filename] = array("saved" => $saved, "upload" => $file, "filename" => $new_filename);
							// add mime type
							if ($mime !== null) $field["mime"] = $mime;
							return SUCCESS;
						}
						else
						{
							foreach ($_FILES as $file)
							{
								if ($file["name"] === $upload)
								{
									// check if error
									$mime = null;
									if (array_key_exists("mime", $input_desc) && $input_desc["mime"] !== "") $mime = $input_desc["mime"];
									$status = $this->check_file($file, $mime);
									if (!($status)) return INVALID_FILE_ERR;
									// check given size
									if (array_key_exists("kb_size", $input_desc) && $input_desc["kb_size"] !== "")
									{
										if (($file['size'] / 1000) > intval($input_desc["kb_size"])) return SIZE_ERR;
									}
									// if no error, add to file list
									$new_filename = sprintf('%s%d', sha1_file($file['tmp_name']), time());
									// check if additional folder
									if (array_key_exists("upload", $input_desc)) $folder = $this->upload_paths[$input_desc["upload"]];
									else $folder = $this->file_upload_dir;
									// set file object
									$this->files[$new_filename] = array("upload" => $file, "saved" => $saved, "folder" => $folder);
									// add new info to field
									$field = array("internal" => $new_filename, "old" => $field["upload"]);
									// add mime type
									if ($mime !== null) $field["mime"] = $mime;
									return SUCCESS;
								}
							}
						}
					}										
					return NO_FILE_ERR;
				case "signature":
					// remove mime type
					$encoded_image = explode(",", $field)[1];
					// decode to image
					$decoded_image = base64_decode($encoded_image);
					// check if successful
					if ($decoded_image === false) return SIGNATURE_DECODE_ERR;
					// check if blank
					$imagick = new Imagick();
					// read blob
					$read_status = $imagick->readImageBlob($decoded_image);
					// check success
					if ($read_status === false) return SIGNATURE_READ_ERR;
					// check if blank
					$colors = $imagick->getImageColors();
					if ((isset($input_desc["mandatory"]) && $input_desc["mandatory"] === true) && !($colors > 1)) return SIGNATURE_COLOR_ERR;
					return SUCCESS;
				default:
					// check if empty string but mandatory
					if ($field === "" && $input_desc["mandatory"] === true) return EMPTY_STR_ERR;
					// get a clean version of the content
					$value = $this->clean_string($field);
					// assign clean value back
					$field = $value;
					return SUCCESS;
			}
		}
		
		/**
		 * Check if there is additional input not part of the input description.
		 * @param input_desc_json the input description json file to be used
		 * @param input the input to be checked
		 * @return true if valid, false otherwise
		 */
		private function check_invalid_input(&$input_desc_json, &$input)
		{
			// check if there are faulty data items
			$names = array_column($input_desc_json, 'name');
			$input_keys = array_keys($input);
			$diff = array_diff($input_keys, $names);
			if ($this->unique_id !== false)
			{
				if (count($diff) == 0) return true;
				if (count($diff) == 1 && $diff[0] !== $this->unique_id) return false;
			}
			else if (count($diff) > 0) return false;
			return true;			
		}
		
		/**
		 * Checks if an input with a given name is empty.
		 * If it is empty, the input is removed. If the entire input array has become empty, it is set null.
		 * @param input a reference to the input array containing the input
		 * @param name the name of the input to check
		 * @return true if input with name given is empty, false otherwise
		 */
		private function check_empty(&$input, $name)
		{
			if (count($input[$name]) < 1)
			{
				unset($input[$name]);
				if (empty($input)) $input = null;
				return true;
			}
			return false;
		}
		
		/**
		 * Checks an input against the description JSON.
		 * @param input_desc_json the input description
		 * @param input the reference to the imput JSON object
		 * @return true if input is valid, false otherwise
		 */
		private function check_input(&$input_desc_json, &$input)
		{			
			// check if there are faulty data items
			if (!($this->check_invalid_input($input_desc_json, $input))) return INVALID_INPUT_ERR;
			
			// loop input description categories
			foreach ($input_desc_json as &$input_desc)
			{
				// get category's name
				$name = $input_desc["name"];
				
				// get mandatory
				$mandatory = false;
				if (isset($input_desc["mandatory"]) && $input_desc["mandatory"] === true)
				{
					$mandatory = true;
				}
				
				// check if mandatory data is missing
				if ((!(isset($input[$name])) || count($input[$name]) < 1) && $mandatory) return MANDATORY_ERR;
				
				// check if input exists
				if (isset($input[$name]))
				{
					if (!($this->check_empty($input, $name)))
					{
						// check each field of input array
						foreach ($input[$name] as &$field)
						{
							// if is category
							if (array_key_exists("inputs", $input_desc))
							{
								// go to next level
								$status = $this->check_input($input_desc["inputs"], $field);								
								if (SUCCESS !== $status) return $status;
								
								// add unique id if set
								if ($this->unique_id !== false && !(array_key_exists($this->unique_id, $field)))
								{
									$field[$this->unique_id] = $this->get_unique_id();
								}
							}
							else // if is input
							{
								$status = $this->check_field($input_desc, $field, $input[$name]);
								if (SUCCESS !== $status) return $status;
							}							
						}
						// remove null values
						$input[$name] = array_filter($input[$name]);
						// check if empty
						$this->check_empty($input, $name);
					}
				}				
			}
			// if no error
			return SUCCESS;
		}
		
		/**
		 * Get a unique hex encoding of a ten bytes random string.
		 * @return the random string
		 */
		private function get_unique_id()
		{
			return bin2hex(random_bytes(10));
		}
	}
?>
